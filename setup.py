#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ast
import os
import setuptools

from setuptools.command.install import install as _install


def get_version_from_init():
    init_file = os.path.join(
        os.path.dirname(__file__), 'pymkitemp', '__init__.py'
    )
    with open(init_file, 'r') as fd:
        for line in fd:
            if line.startswith('__version__'):
                return ast.literal_eval(line.split('=', 1)[1].strip())


# Custom install function to install and register with cmmnbuild-dep-manager
class install(_install):

    def run(self):
        try:
            import pagestore
            import pip
            print('WARNING: removing standalone pagestore package')
            pip.main(['uninstall', 'pagestore', '-y'])
        except:
            pass
    
        import cmmnbuild_dep_manager
        mgr = cmmnbuild_dep_manager.Manager()
        mgr.install('pymkitemp')
        print('registered pymkitemp with cmmnbuild_dep_manager')
        _install.run(self)


setuptools.setup(
    name='pymkitemp',
    version=get_version_from_init(),
    description='Python analysis of longterm MKI data',
    author='Agnieszka Chmielinska',
    author_email='agnieszka.chmielinska@cern.ch',
    url='https://gitlab.cern.ch/achmieli/pymkitemp',
    packages=['pymkitemp'],
    install_requires=[
        'pytimber>=2.5.0',
        'cmmnbuild-dep-manager>=2.1.2',
    ],
    cmdclass={
        'install': install
    }
)
