# PyMKITemp

Detecting SS events.
Long-term analysis of the Soft Start end:
- Average risetime
- Average delay time
- Average delay time CP
- Average temperature at MKI upstream end
- Average temperature at MKI downstream end


Polls PyTimber, Pagestore, PjLSA.

Monitoring of MKI temperetures and relative temperatures (with respect to MKI SIS interlocks).

## Installation

Accessibly by everyone.
Requires ANACONDA (Python 3.6) https://www.continuum.io/downloads 
```
pip install git+https://gitlab.cern.ch/scripting-tools/cmmnbuild-dep-manager.git
pip install pytimber
pip install pjlsa
pip install git+https://gitlab.cern.ch/achmieli/PyMKITemp.git
git clone https://gitlab.cern.ch/achmieli/PyMKITemp.git


cd PyMKITemp
```

## Updating this repository

If changing this repository, please bump the version. See example diff:
https://gitlab.cern.ch/achmieli/PyMKITemp/commit/e2f0b41412eaed9d9b41b95e1be4b0e54f23d6e1

## Downloading latest version

```git pull ```

run ```pip install .``` in the root of the project 
### Getting Started

For data import put the dates (do not stop the programe while running - may lead to faultly working of Pagestore)
```
python pymkitemp/import_data_local_db.py  
```

For SS analysis, temperature and relative temperature monitoring:
```
python pymkitemp/upload_data_TimeWindow2.py
```
