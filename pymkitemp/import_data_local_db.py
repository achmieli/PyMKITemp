import pytimber
from pytimber import pagestore
import time, calendar
import beam_spec
import numpy as np
import pytz
from datetime import datetime
ldb = pytimber.LoggingDB(source='all')
mydb = pagestore.PageStore('mydata.db', './../../MKITempdata')

utc = pytz.utc
cet = pytz.timezone('CET')


t1 = '2017-02-01 00:00:00' #(input("Start date? (ex.2017-07-25 10:15:00.000)\n"))
t2 = '2017-09-01 00:00:00'
ts1 = cet.localize(datetime.strptime(t1,"%Y-%m-%d %H:%M:%S")).astimezone(utc).timestamp()
ts2 = cet.localize(datetime.strptime(t2,"%Y-%m-%d %H:%M:%S")).astimezone(utc).timestamp()

keys = beam_spec.get_all_keys()

print(keys)

timber_data = ldb.get(keys, ts1, ts2)
mydb.store(timber_data)


