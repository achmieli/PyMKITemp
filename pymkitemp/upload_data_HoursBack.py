import pytimber
import matplotlib.pyplot as plt
import matplotlib.dates as md
import time
import datetime
import numpy
import numpy as np
import numpy.polynomial.polynomial as poly
from numpy.polynomial import Polynomial
from scipy.interpolate import interp1d

ldb = pytimber.LoggingDB()


def splitArray(arr, arr2, distance): # To divide filtered unzipped_timeIP to smaller vectors corresponding to the event
    index = 1            # Python's indexing starts at zero
    events = [];
    events2 = [];
    current_event = [arr[0]]
    current_event2 = [arr2[0]]
    while index < len(arr):
        if arr[index]- arr[index - 1] > distance:
            events.append(current_event)
            events2.append(current_event2)
            current_event = []
            current_event2 = []
        current_event.append(arr[index])
        current_event2.append(arr2[index])
        index += 1
    events.append(current_event)
    events2.append(current_event2)
    return events, events2

"""
arr = [80, 82, 2000, 2002]
arr2 = ["a", "b", "c", "d"]
distance = 1000
cos=(splitArray(arr,arr2, distance))
print(cos[0][0])
print(cos[0][1])
print(cos[1][0])
print(cos[1][1])"""


beam = (input("Which Beam?\n"))
if beam=="B1":
    magnet = (input("Which Magnet?\n"))
    if magnet=="A" or magnet=="B" or  magnet=="C" or  magnet=="D":
            delay_name = "MKI.UA23.IPOC.%B1:T_DELAY"
            strength_name = "MKI.UA23.IPOC.%B1:I_STRENGTH"
            length_name = "MKI.UA23.IPOC.%B1:T_LENGTH" 
            risetime_name="MKI.UA23.IPOC.%B1:T_RISETIME"
            temperature_name="MKI.%5L2.B1:TEMP_MAGNET_UP"
            delay_min_filter=50
            delay_max_filter=56
            strength_filter=4.8
            length_filter=7.3
            pass
    else: print("Wrong name of the magnet")
elif beam=="B2":
    magnet = (input("Which Magnet?\n"))
    if magnet=="A" or magnet=="B" or  magnet=="C" or  magnet=="D":
            delay_name = "MKI.UA87.IPOC.%B2:T_DELAY"
            strength_name = "MKI.UA87.IPOC.%B2:I_STRENGTH"
            length_name = "MKI.UA87.IPOC.%B2:T_LENGTH" 
            risetime_name="MKI.UA87.IPOC.%B2:T_RISETIME"
            temperature_name="MKI.%5R8.B2:TEMP_MAGNET_UP"
            delay_min_filter=50
            delay_max_filter=56
            strength_filter=5.0
            length_filter=7.3
            pass
    else: print("Wrong name of the magnet")
else: print("Wrong beam")
#t1 = '2017-07-25 10:15:00.000'
#t2 = '2017-07-26 10:15:00.000'

now=time.time()
#import from last 24 hours
nb_hours = int(input("How many hours back?\n"))
now_minus_a_day = now - 3600*nb_hours
delay_key= delay_name.replace("%",magnet)
strength_key=strength_name.replace("%",magnet)
length_key=length_name.replace("%",magnet)
risetime_key=risetime_name.replace("%",magnet)
temperature_key=temperature_name.replace("%",magnet)
#print(data_delay, data_strength)

#ts1 = '2017-07-31 09:10:00'
#ts2 = '2017-08-01 09:10:00'

#time=data_delay[delay_key][0]
#delay=data_delay[delay_key][1]
#"""time2=data_strength[strength_key][0]"""
#strength=data_strength[strength_key][1]
#temperaturetime=data_temperature[temperature_key][0]
#temperature=data_temperature[temperature_key][1]

data=ldb.get([delay_key,strength_key,length_key, risetime_key, temperature_key],now_minus_a_day,now)
#data=ldb.get([delay_key,strength_key,length_key, risetime_key, temperature_key],ts1,ts2)

timeIP=data[delay_key][0] #time of delay, rise time etc measurement
delay=data[delay_key][1]
strength=data[strength_key][1]
length=data[length_key][1]
risetime=data[risetime_key][1]

timeTP=data[temperature_key][0]
temp=data[temperature_key][1]

plt.figure(figsize=(12,6))
plt.plot(timeIP,delay, 'o',label=r'delay')
plt.plot(timeIP,strength, 'o',label=r'strength')
plt.plot(timeIP,length, 'o',label=r'pulse length')
plt.plot(timeIP,risetime, 'o',label=r'rise time')
plt.plot(timeTP,temp, 'o',label=r'temperature')
plt.legend()
plt.title(time.asctime(time.localtime(now)))
pytimber.set_xaxis_date()
plt.show()


zipped=list(zip(timeIP,delay,strength,length,risetime))

print(zipped)

filtered=list(filter(lambda x: x[1]>delay_min_filter and x[1]<delay_max_filter and x[2]>strength_filter and x[3]>length_filter, zipped))





print(filtered)

unzipped_timeIP, unzipped_delay, unzipped_strength, unzipped_length, unzipped_risetime =zip(*filtered)

f = interp1d(timeTP, temp)

#print(unzipped_timeIP)
#print(unzipped_risetime)

distance = 1000
cos=splitArray(unzipped_timeIP, unzipped_risetime, distance)
print("Number of vectors:")
print(len(cos[0]))



plt.figure(figsize=(12,6))
plt.plot(unzipped_timeIP,unzipped_delay, 'o',label=r'delay')
plt.plot(unzipped_timeIP,unzipped_strength, 'o',label=r'strength')
plt.plot(unzipped_timeIP,unzipped_length, 'o',label=r'pulse length')
plt.plot(unzipped_timeIP,unzipped_risetime, 'o',label=r'rise time')
plt.plot(timeTP,temp, 'o',label=r'temperature') 

mean_temperatures=[]
mean_risetimes=[]
for i in range(len(cos[0])):
    temp_estimate=(f(np.array(cos[0][i])))
    plt.plot(cos[0][i],temp_estimate, 'o',label=r'temperature interpolation')
    mean_temp=np.mean(temp_estimate)
    mean_risetime=np.mean(cos[1][i])
    mean_temperatures.append(mean_temp)
    mean_risetimes.append(mean_risetime)
plt.legend()

print(mean_temperatures)
print(mean_risetimes)
"""
print(mean_temperatures)
#x = datetime.datetime.strptime(ts2, '%Y-%m-%d %H:%M:%S')
#t = time.mktime(time.strptime(ts2, '%Y-%m-%d %H:%M:%S'))
#cos=datetime.datetime.fromtimestamp(float(ts2)).strftime('%Y-%m-%d %H:%M:%S')
#print(t)"""


plt.title(time.asctime(time.localtime(now)))
pytimber.set_xaxis_date()
plt.show()

plt.figure(figsize=(12,6))
plt.plot(mean_temperatures,mean_risetimes, 'o', color='navy')


ax=plt.gca()
coefs = numpy.polyfit(mean_temperatures, mean_risetimes, 1)
polynomial = numpy.poly1d(coefs)
equation = 'y = ' + str((coefs[0])) + ' x + ' + str((coefs[1])) 
print(equation )
ax.text(0.95, 0.95,equation,horizontalalignment='right', verticalalignment='top', transform = ax.transAxes)
x=np.linspace(min(mean_temperatures), max(mean_temperatures), 100)
plt.plot(x, polynomial(x),  color='red' )
print(polynomial)
ax.set_xlabel(r'Temperature [$^\circ$C]')
ax.set_ylabel(r'Rise Time [$\mu$s]')
plt.title("Magnet "+ magnet)
plt.show()


# fit_fn is now a function which takes in x and returns an estimate for y



#newlist=[]
#for i in range(len(temperaturetime)):
# cos=datetime.datetime.fromtimestamp(float(temperaturetime[i])).strftime('%Y-%m-%d %H:%M:%S')
#  newlist.append(cos)
    
#x = [datetime.datetime.strptime(elem, '%Y-%m-%d %H:%M:%S') for elem in newlist]
#xs = matplotlib.dates.date2num(x)



#ax=plt.gca()
#matplotlib.rcParams.update({'font.size': 12, 'font.family': 'sans-serif'})
#xfmt = md.DateFormatter('%Y-%m-%d\n%H:%M:%S')
#ax.xaxis.set_major_formatter(xfmt)
#plt.plot(xs,temperature, "ko")

#ax.set_ylabel(r'Temperature [$^\circ$C]')
#ax.legend( ('MKI.D5L2.B1:TEMP_MAGNET_UP'), loc='lower center')
#plt.show()

