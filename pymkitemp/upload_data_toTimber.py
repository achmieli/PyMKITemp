import pytimber
import time, calendar
import numpy as np
import beam_spec
import rise_time_vs_temperature
from datetime import datetime

ldb = pytimber.LoggingDB()

now=int(time.time())
before=int(time.time()-300000)
  
    
    
t1 = str(datetime.fromtimestamp(before))
t2 = str(datetime.fromtimestamp(now))

print(t1,t2)
beam="B2"
magnet='D'

result=rise_time_vs_temperature.last_event(ldb, beam, magnet, t1, t2, 'rise_time', 'UP', show_plots=False)
result2=rise_time_vs_temperature.last_event(ldb, beam, magnet, t1, t2, 'delay_time', 'DOWN', show_plots=False)
mean_temp_up=result[0]
mean_temp_down=result2[0]
mean_risetime=result[1]
last_element_time=result[2]
mean_delaytime=result2[1]
if result and result2:
    print('Average rise time= ', mean_risetime)
    print('Average delay time= ', mean_delaytime)
    print('Average temperature UP= ', mean_temp_up)
    print('Average temperature DOWN= ', mean_temp_down)
    print("Last element event time: ", str(datetime.fromtimestamp(result[2])))
else:  #if there was no event, so there is no time to print   
    pass

relative=rise_time_vs_temperature.relative_temp(ldb, beam, magnet, 'UP', t1, t2)
relative2=rise_time_vs_temperature.relative_temp(ldb, beam, magnet, 'DOWN', t1, t2)


print(relative, relative2)

for magnet in beam_spec.magnets:
    for beam in beam_spec.beams:
        for side in beam_spec.sides:
            print(beam, magnet, side, beam_spec.get_threshold(beam, magnet, side))
# ts1 = calendar.timegm(time.strptime(t1,"%Y-%m-%d %H:%M:%S"))-2*3600
# ts2 = calendar.timegm(time.strptime(t2,"%Y-%m-%d %H:%M:%S"))-2*3600



