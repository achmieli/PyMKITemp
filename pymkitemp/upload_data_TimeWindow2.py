import rise_time_vs_temperature
import pymkitemp
import time
from datetime import datetime
from beam_spec import beam_to_spec
import beam_spec



t1 = '2017-06-23 00:00:00' #(input("Start date? (ex.2017-07-25 10:15:00.000)\n"))
t2 = '2017-06-24 00:00:00'
# for magnet in beam_spec.magnets:
#     for beam in beam_spec.beams:
beam="B2"
magnet='D'
mean_temperatures, mean_risetimes=rise_time_vs_temperature.analyse_all(beam, magnet, t1, t2, 'delay_time', 'UP', show_plots = False)
 
 
#import pjlsa
#beam="B1"
#lsa = pjlsa.LSAClient()
#parameter="MKITempMagnetLimits%s/Temp#limits" % (beam)
#trims = lsa.getLastTrim(beamprocess="_NON_MULTIPLEXED_LHC", parameter=parameter, part='value')
