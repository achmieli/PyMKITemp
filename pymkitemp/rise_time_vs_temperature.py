import pytimber
import matplotlib.pyplot as plt
import time, calendar
import numpy as np
import numpy
import pymkitemp.beam_spec
from pytimber import pagestore
from scipy.interpolate import interp1d
from decimal import Decimal
from collections import defaultdict
from datetime import datetime
import pytz

BIG_FONT_SIZE = 15;
LEGEND_FONT_SIZE=13;
MARKER_SIZE = 2
plt.rc('font',**{'family':'sans-serif'})
plt.rcParams.update({'font.size': LEGEND_FONT_SIZE})
plt.rc('xtick', labelsize=BIG_FONT_SIZE) 
plt.rc('ytick', labelsize=BIG_FONT_SIZE) 
plt.rcParams["lines.markersize"] = MARKER_SIZE 
plt.rcParams["figure.figsize"] = [12,6]
plt.rcParams['lines.linewidth'] = 2
plt.rcParams['axes.labelsize']=BIG_FONT_SIZE
plt.rcParams['axes.titlesize']=BIG_FONT_SIZE
plt.rcParams['axes.linewidth'] = 1.5
plt.rcParams['xtick.direction']='in'
plt.rcParams['ytick.direction']='in'
plt.rcParams['xtick.minor.visible']=True
plt.rcParams['ytick.minor.visible']=True 
plt.rcParams['xtick.major.size']=10
plt.rcParams['xtick.minor.size']=6
plt.rcParams['ytick.major.size']=10 
plt.rcParams['ytick.minor.size']=6
plt.rcParams['xtick.major.width']=1.5 
plt.rcParams['xtick.minor.width']=1.5 
plt.rcParams['ytick.major.width']=1.5
plt.rcParams['ytick.minor.width']=1.5
plt.rcParams['axes.grid']=False








def analyse_all(beam, magnet, t1, t2, parameter, temp_side, show_plots = False): #parameter = 'rise_time' or 'delay_time'
    mydb = pagestore.PageStore('mydata.db', './../../MKITempdata')
    analyse_imported_data(mydb, beam, magnet, t1, t2, temp_side, show_plots) 
    mean_temperatures, mean_datatimes, last_element_times = analyse_parameter_vs_temp(mydb, beam, magnet, t1, t2, parameter, temp_side,  show_plots) 
    analyse_threshold_data(mydb, beam, magnet, temp_side, t1, t2)
    analyse_main_plot(beam, magnet, mean_temperatures,mean_datatimes, last_element_times, t1, t2, parameter, temp_side)
    return mean_temperatures, mean_datatimes  #added to analyse rt vs temp for periods

def last_event(data_source, beam, magnet, t1, t2, parameter, temp_side, show_plots = False):
    mean_temperatures,  mean_datatimes, last_element_times = analyse_parameter_vs_temp(data_source, beam, magnet, t1, t2, parameter,temp_side, show_plots)     
    if not mean_temperatures: #if sth is empty
        return None
    return mean_temperatures[-1],  mean_datatimes[-1], last_element_times[-1]

def relative_temp(data_source, beam, magnet, temp_side, threshold, t1, t2):
    timeIP, delay, strength, length, risetime, delay_cp_up, delay_cp_dw, timeTP, temp = read_data(data_source, beam, magnet, temp_side, t1, t2)
    average = np.mean((temp))
    relative=100*average/threshold
    return relative

def read_data(data_source, beam, magnet, temp_side, t1, t2):
    delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key =pymkitemp.beam_spec.get_keys(beam, magnet, temp_side)
    print(delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key)
    utc = pytz.utc
    cet = pytz.timezone('CET')
    ts1 = cet.localize(datetime.strptime(t1,"%Y-%m-%d %H:%M:%S")).astimezone(utc).timestamp()
    ts2 = cet.localize(datetime.strptime(t2,"%Y-%m-%d %H:%M:%S")).astimezone(utc).timestamp()
    data=data_source.get([delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key],ts1,ts2)
    timeTP=data[temperature_key][0] #timeTP-> time of temperature data
    temp=data[temperature_key][1]
    
    zipped= list(zip(timeTP, temp))
    temp_filtered=list(filter(lambda x: x[1]<500, zipped))
   
    if temp_filtered:
        timeTP, temp= zip(*temp_filtered)
    else:
        timeTP=  temp=[]
    
    d1 = dict(zip(data[delay_key][0], data[delay_key][1])); 
    d2 = dict(zip(data[strength_key][0], data[strength_key][1]));
    d3 = dict(zip(data[length_key][0], data[length_key][1])); 
    d4 = dict(zip(data[risetime_key][0], data[risetime_key][1]));
    d5 = dict(zip(data[delay_cp_up_key][0], data[delay_cp_up_key][1]));
    d6 = dict(zip(data[delay_cp_dw_key][0], data[delay_cp_dw_key][1]));
#     print("d1 keys", d1.keys())
#     print("d5 keys", d5.keys())
    if len(d5) != len(d1):
        for freq, val in d1.items():
            if not (freq in d5):
                d5[freq] = 0
            if not (freq in d6):
                d6[freq] = 0
    
    dd = defaultdict(list);

    for d in (d1, d2, d3, d4, d5, d6): # you can list as many input dicts as you want here
        for key, value in d.items():
            dd[key].append(value)

    dd_filtered = {k: v for k, v in dd.items() if len(v) == 6 and v[1]<100 and v[1]>0 and v[3]<1}
    timeIP= list(dd_filtered.keys())
    if dd_filtered: #if not empty
        delay, strength, length, risetime, delay_cp_up, delay_cp_dw =zip(*dd_filtered.values())
    else:
        delay = strength = length = risetime = delay_cp_up = delay_cp_dw = []
        
    return timeIP, delay, strength, length, risetime, delay_cp_up, delay_cp_dw, timeTP, temp

def analyse_imported_data(data_source, beam, magnet, t1, t2, temp_side, show_plots = False):   
    delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key = pymkitemp.beam_spec.get_keys(beam, magnet, temp_side)
    now=time.time()
    print("Beam: ", beam)
    print("Magnet: ", magnet)
    
    timeIP, delay, strength, length, risetime, delay_cp_up, delay_cp_dw, timeTP, temp = read_data(data_source, beam, magnet, temp_side, t1, t2)
    
    
#FIGURE: IMPORTED DATA (from LDB_PRO)
    if show_plots==True:
        plt.figure(figsize=(12,6))
        plt.plot(timeIP, delay_cp_up, 'o', color = 'darkgrey', markersize=8, markeredgecolor='dimgrey', markeredgewidth=1.0, label=delay_cp_up_key)
        plt.plot(timeIP, delay_cp_dw, 'o', color = 'mediumslateblue', markersize=8, markeredgecolor='darkblue', markeredgewidth=1.0, label=delay_cp_dw_key)
        plt.plot(timeIP,delay, 'o',  color='dodgerblue', markersize=8, markeredgecolor='b', markeredgewidth=1.0, label=delay_key)
        plt.plot(timeIP,strength, 'o',   color='lightgreen', markersize=8,  markeredgecolor='forestgreen', markeredgewidth=1.0, label=strength_key)
        plt.plot(timeIP,length, 'o',  color='gold', markersize=8, markeredgecolor='darkorange', markeredgewidth=1.0, label=length_key)
        plt.plot(timeIP,risetime, 'o',  color='plum',markersize=8, markeredgecolor='magenta', markeredgewidth=1.0, label= risetime_key)
        plt.plot(timeTP,temp, 'o', color='lightcoral', markersize=8, markeredgecolor='red', markeredgewidth=1.0, label=temperature_key)
        plt.legend()
        plt.title(temperature_key + ": LDB_PRO" + "\n" + "(" + t1 +" to " + t2 + ")" ) 
        pytimber.set_xaxis_date()
        plt.show()
    
 
 
def analyse_data_vs_temp(beam, magnet, filtered, timeTP, temp, t1, t2, parameter, temp_side,  show_plots=False): #unzipped_rise time or unzipped_delay
    now=time.time()
    mean_temperatures=[]
    mean_datatimes=[]
    last_element_times=[]
    
    if not filtered:
        return mean_temperatures, mean_datatimes, last_element_times
    
    unzipped_timeIP, unzipped_delay, unzipped_strength, unzipped_length, unzipped_risetime, unzipped_delay_cp_up, unzipped_delay_cp_dw =zip(*filtered)
    
    unzipped_delay_cp=np.subtract(unzipped_delay_cp_dw, unzipped_delay_cp_up)
    if parameter=="rise_time": 
        unzipped_data=unzipped_risetime
    elif  parameter=="delay_time":
        unzipped_data=unzipped_delay
    elif parameter=="delay_time_CP":
        unzipped_data=unzipped_delay_cp
    
    distance = 500 # 500s reasonable value to distinguish events:
    #this value must be greater then the distance between successive points (10s)
    #this value must be smaller then the distance between last SS end and next SS end beginning 
    cos=pymkitemp.beam_spec.splitArray(unzipped_timeIP, unzipped_data, distance) # dividing vector into smaller vector

    
    delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key = pymkitemp.beam_spec.get_keys(beam, magnet, temp_side)
   
  
    
    
    # FIGURE: FILTERED DATA
    if show_plots == True:
        plt.figure(figsize=(12,6))
        plt.plot(unzipped_timeIP, unzipped_delay_cp, 'o', color = 'grey', markersize=8, markeredgecolor='black', markeredgewidth=1.0, label=delay_cp_up_key+"-"+delay_cp_dw_key)
        plt.plot(unzipped_timeIP,unzipped_delay, 'o',  color='dodgerblue', markersize=8, markeredgecolor='b', markeredgewidth=1.0, label=delay_key)
        plt.plot(unzipped_timeIP,unzipped_strength, 'o',   color='lightgreen', markersize=8,  markeredgecolor='forestgreen', markeredgewidth=1.0, label=strength_key)
        plt.plot(unzipped_timeIP,unzipped_length, 'o',  color='gold', markersize=8, markeredgecolor='darkorange', markeredgewidth=1.0, label=length_key)
        plt.plot(unzipped_timeIP,unzipped_risetime, 'o',  color='plum',markersize=8, markeredgecolor='magenta', markeredgewidth=1.0, label= risetime_key)
        plt.plot(timeTP,temp, 'o', color='lightcoral', markersize=8, markeredgecolor='red', markeredgewidth=1.0, label=temperature_key)
      
    # INTERPOLATION
    f = interp1d(timeTP, temp)
    
    
    for i in range(len(cos[0])): #for each event
        if len(cos[0][i])<8: # we do not consider events with less than 8 points (since 1 year it was 9/18/28 points)
            continue
        
        if cos[0][i][0] < timeTP[0]:  #Kiedy sie zaczal ity event jest mniejszy od poczatku czasu tmeperatury
            # If first element of i-th event of unzipped_timeIP is before temperature time
            mean_temp=temp[0] 
            
        elif cos[0][i][-1]> timeTP[-1]:
            mean_temp=temp[-1] 
            
            # or if last element of i-th event of unzipped_timeIP is above temperature time
        else:     
            temp_estimate=(f(np.array(cos[0][i])))
            if show_plots == True: plt.plot(cos[0][i],temp_estimate, 'o', markersize=8, markeredgewidth=1.0)
            mean_temp=np.mean(temp_estimate)
        last_element_time=cos[0][i][-1]
        #print("Event:" +str(i)+ "Number of pulses:" + str(len(cos[0][i])) ) #check how many points is in event
#         if i+1<len(cos[0]):
#             print("Distance between end of last event to the begining of new event: "+ str(cos[0][i+1][0]-cos[0][i][-1]))
        mean_datatime=np.mean(cos[1][i])
        mean_temperatures.append(mean_temp)
        mean_datatimes.append(mean_datatime)
        last_element_times.append(last_element_time)
    if show_plots == True:    
        #plt.legend()
        plt.title(temperature_key + ": Filtered Data" +"\n" + "(" + t1 +" to " + t2 + ")"  ) 
        pytimber.set_xaxis_date()
        plt.ylabel("Value [a.u.]")
        plt.xlabel("Time")
        plt.show()
    return mean_temperatures, mean_datatimes, last_element_times 
 

 
    
def analyse_filtered_data(data_source, beam, magnet, temp_side, t1, t2):   
         
    timeIP, delay, strength, length, risetime, delay_cp_up, delay_cp_dw, timeTP, temp = read_data(data_source, beam, magnet, temp_side, t1, t2) 

    # Tuples
    zipped=list(zip(timeIP,delay,strength,length,risetime, delay_cp_up, delay_cp_dw))
    # FILTERING IPOC DATA
    delay_min_filter, delay_max_filter, strength_filter, length_filter = pymkitemp.beam_spec.get_filters(beam)
    filtered=list(filter(lambda x: x[1]>delay_min_filter and x[1]<delay_max_filter and x[2]>strength_filter and x[3]>length_filter, zipped))
    return filtered, timeTP, temp
        
      

def analyse_parameter_vs_temp(data_source, beam, magnet, t1, t2, parameter, temp_side, show_plots = False):
    filtered, timeTP, temp=analyse_filtered_data(data_source, beam, magnet, temp_side, t1, t2)
    mean_temperatures, mean_datatimes, last_element_times=analyse_data_vs_temp(beam, magnet, filtered, timeTP, temp, t1,t2, parameter, temp_side, show_plots)
    return mean_temperatures, mean_datatimes, last_element_times

def analyse_threshold_data(data_source, beam, magnet, temp_side, t1, t2):
    now=time.time()
    delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key = pymkitemp.beam_spec.get_keys(beam, magnet, temp_side)
    timeIP, delay, strength, length, risetime, delay_cp_up, delay_cp_dw, timeTP, temp = read_data(data_source, beam, magnet, temp_side, t1, t2)
   
    
    f = interp1d(timeTP, temp)
    N=10000
    arg = np.linspace(timeTP[0], timeTP[-1], N)


    threshold=pymkitemp.beam_spec.get_threshold(beam, magnet, temp_side)
    print("Threshold: ", threshold)
    # FIGURE: Threshold
    
    plt.figure(figsize=(12,6))
    plt.plot(arg,f(arg), 'o-', color='red', markersize=3, label=temperature_key)
    temp=f(arg)
    temp_th = 100*np.array(temp)/threshold
    
    plt.plot(arg, temp_th, 'o-', color='black', markersize=3, label="Threshold:" + str(threshold) + "$^\circ$C")
    plt.legend()
    plt.title(temperature_key + "\n" +"(" + t1 +" to " + t2 + ")"  ) 
    pytimber.set_xaxis_date()
    plt.show()
    
    
    plt.figure(figsize=(12,6))
    plt.plot(arg,f(arg), 'o', color='lightcoral', markersize=8, markeredgecolor='red', markeredgewidth=1.0, label=temperature_key)
    
    temp_th = [threshold] * len(arg)
    temp_th2 = [62.7] * len(arg)

    plt.plot(arg, temp_th, linestyle='solid', color='black', linewidth=3, label="Threshold: " + str(threshold) + "$^\circ$C")
    plt.plot(arg, temp_th2, linestyle='solid', color='grey', linewidth=3, label="Threshold: " + str(62.7) + "$^\circ$C")

    plt.legend()
    plt.title(temperature_key + "\n" +"(" + t1 +" to " + t2 + ")"  ) 
    pytimber.set_xaxis_date()
    plt.ylabel(r'Temperature [$^\circ$C]')
    plt.xlabel("Time")
    plt.show()
        
def analyse_main_plot(beam, magnet, mean_temperatures,mean_datatimes, last_element_times, t1,t2, parameter, temp_side):
    
    now=time.time()
    #print(mean_temperatures)
    delay_key,strength_key,length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key = pymkitemp.beam_spec.get_keys(beam, magnet, temp_side)
    # FIGURE: MAIN PLOT
    plt.figure(figsize=(12,6))
    plt.plot(mean_temperatures,mean_datatimes,  'o', color='fuchsia', markersize=8, markeredgecolor='darkviolet', markeredgewidth=1.0, label="Average rise time during SS end")
    ax=plt.gca()
    plt.legend()
    division=pymkitemp.beam_spec.get_division(beam, magnet)
     
    zipped=list(zip(mean_temperatures, mean_datatimes))
    first=list(filter(lambda x: x[0]<division, zipped))
    #if first:
    #    first_mean_temperatures, first_mean_datatimes =zip(*first)
    #    polynomial1, equation1 = adjust_fit(first_mean_temperatures, first_mean_datatimes)
    #    x1=np.linspace(min(first_mean_temperatures), max(first_mean_temperatures), 100)
    #    plt.plot(x1, polynomial1(x1),  color='blue' , label=equation1)
    #    print("FIT 1: " , polynomial1)
     
    #second=list(filter(lambda x: x[0]>=division, zipped))
    #if second:
    #    second_mean_temperatures, second_mean_datatimes =zip(*second)
    #    polynomial2, equation2 = adjust_fit(second_mean_temperatures, second_mean_datatimes)
     #   x2=np.linspace(min(second_mean_temperatures), max(second_mean_temperatures), 100)
    #    plt.plot(x2, polynomial2(x2),  color='green' , label=equation2)
    #    print("FIT 2: " , polynomial2)
     
    ax.set_xlabel(r'Average temperature [$^\circ$C]')
    ax.set_ylabel(parameter + ' [$\mu$s]')
    plt.title(temperature_key  +"\n" + "(" + t1 +"to" + t2 + ")" ) 
#     print("Maximum temperature: " + str(max(mean_temperatures)))
    plt.legend()
    plt.show()
    
    # FIGURE: MAIN PLOT 2
    plt.figure(figsize=(12,6))
    plt.plot(last_element_times,mean_datatimes, 'o', color='fuchsia', markersize=8, markeredgecolor='darkviolet', markeredgewidth=1.0)
    ax=plt.gca()  
    
    
    if parameter=="rise_time": 
        tit="Average rise time"
    elif  parameter=="delay_time":
        tit="Average delay time with respect to RF pre-pulse"
    elif parameter=="delay_time_CP":
        tit=delay_cp_up_key + "-" +  delay_cp_dw_key 
        plt.gca().invert_yaxis()
    
    
    plt.title(tit +"\n" + "(" + t1 +" to " + t2 + ")" ) 

    #plt.title(delay_cp_up_key + "-" +  delay_cp_dw_key +"\n" + "(" + t1 +" to " + t2 + ")" ) 
    ax.set_xlabel(r'Local time')
    ax.set_ylabel(parameter + ' [$\mathrm{\mu}$s]')  
    pytimber.set_xaxis_date()
    plt.show()

def adjust_fit(mean_temperatures,  mean_datatimes):
    coefs = numpy.polyfit(mean_temperatures, mean_datatimes, 1)
    polynomial = numpy.poly1d(coefs)
    equation = 'FIT: y = ' + str(round(Decimal(coefs[0]), 9)) + ' x + ' + str(round(Decimal(coefs[1]), 5))
    return polynomial, equation