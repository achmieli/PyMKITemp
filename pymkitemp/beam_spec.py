from __future__ import division
import numpy as np
import pjlsa
lsa = pjlsa.LSAClient()
beams = ['B1','B2']
beam_to_spec = {
    'B1': {
        'mki_name': '23',
        'temp_name': 'L2',
        'delay_min': 50,
        'delay_max': 56,
        'strength': 4.8,
        'length': 4.3,
        'threshold': {
            'A': 54,
            'B': 52,
            'C': 54,
            'D': 54,
        },
        'division': {
            'A': 50,
            'B': 50,
            'C': 50,
            'D': 50,
        }
    },
    'B2': {
        'mki_name': '87',
        'temp_name': 'R8',
        'delay_min': 50,
        'delay_max': 56,
        'strength': 5.0,
        'length': 4.3,
        'threshold': {
            'A': 55.5,
            'B': 52.8,
            'C': 53.4,
            'D': 62.7,            
        },
        'division': {
            'A': 50,
            'B': 50,
            'C': 50,
            'D': 50,
        }
    }
}
magnets = ['A', 'B', 'C', 'D']
sides = ['UP', 'DOWN']

def splitArray(arr, arr2, distance): #  To divide filtered unzipped_timeIP to smaller vectors corresponding to the event
    index = 1            # Python's indexing starts at zero
    events = [];
    events2 = [];
    current_event = [arr[0]]
    current_event2 = [arr2[0]]
    while index < len(arr):
        if arr[index]- arr[index - 1] > distance:
            events.append(current_event)
            events2.append(current_event2)
            current_event = []
            current_event2 = []
        current_event.append(arr[index])
        current_event2.append(arr2[index])
        index += 1
    events.append(current_event)
    events2.append(current_event2)
    return events, events2

def reduceArray(myarray,k,n): # delate every k element, repeat n-times
    for i in range(1,n):
        myarray = np.delete(myarray, np.arange(0, myarray.size, k))
    return myarray


def get_keys(beam, magnet, temp_side='UP'):
   
    delay_key = "MKI.UA%s.IPOC.%s%s:T_DELAY" % (beam_to_spec[beam]['mki_name'], magnet, beam)
    strength_key = "MKI.UA%s.IPOC.%s%s:I_STRENGTH" % (beam_to_spec[beam]['mki_name'], magnet, beam)
    length_key = "MKI.UA%s.IPOC.%s%s:T_LENGTH" % (beam_to_spec[beam]['mki_name'], magnet, beam)
    risetime_key="MKI.UA%s.IPOC.%s%s:T_RISETIME" % (beam_to_spec[beam]['mki_name'], magnet, beam)
    temperature_key="MKI.%s5%s.%s:TEMP_MAGNET_%s" % (magnet, beam_to_spec[beam]['temp_name'], beam, temp_side)
    delay_cp_up_key = "MKI.UA%s.IPOC.CPU-%s.%s%s:T_TH" % (beam_to_spec[beam]['mki_name'], 'UP', magnet, beam)
    delay_cp_dw_key = "MKI.UA%s.IPOC.CPU-%s.%s%s:T_TH" % (beam_to_spec[beam]['mki_name'], 'DW', magnet, beam)
    return delay_key, strength_key, length_key, risetime_key, temperature_key, delay_cp_up_key, delay_cp_dw_key

def get_all_keys():
       
    keys_all=[]
    for magnet in magnets:
        for beam in beams:
            for side in sides:
                keys = get_keys(beam, magnet, side)
                for x in keys:
                    keys_all.append(x) 
    return keys_all
    

def get_filters(beam):
    delay_min_filter=beam_to_spec[beam]['delay_min']
    delay_max_filter=beam_to_spec[beam]['delay_max']
    strength_filter=beam_to_spec[beam]['strength']
    length_filter=beam_to_spec[beam]['length']
    return delay_min_filter, delay_max_filter, strength_filter, length_filter

def get_threshold(beam, magnet, side):
    tempLimits=get_beam_thresholds(beam)
    index=ord(magnet) - ord('A')
    isDown = 1 if side == "DOWN" else 0
    return tempLimits[2 * index + isDown]

def get_beam_thresholds(beam):
    return [50]*8
    parameter="MKITempMagnetLimits%s/Temp#limits" % (beam)
    trims = lsa.getLastTrim(beamprocess="_NON_MULTIPLEXED_LHC", parameter=parameter, part='value')
    (timetrim, data) = trims
    value = data.getArrayValue()
    tempLimits = value.getDoubles()
    return tempLimits 

def get_threshold_fast(magnet, side, tempLimits):
    index=ord(magnet) - ord('A')
    isDown = 1 if side == "DOWN" else 0
    return tempLimits[2 * index + isDown]
    #return beam_to_spec[beam]['threshold'][magnet] #if one want to use fixed thresholds
    
def get_division(beam, magnet):
    return beam_to_spec[beam]['division'][magnet]
