import rise_time_vs_temperature
import beam_spec
import matplotlib.pyplot as plt
import pytimber


t1 = '2017-08-03 00:00:00'
t2 = '2017-08-10 08:00:00'




# for magnet in beam_spec.magnets:
#     for beam in beam_spec.beams:
beam="B2"
magnet='D'
#mean_temperatures, mean_risetimes=rise_time_vs_temperature.analyse_all(beam, magnet, t1, t2)

 
tA1 = '2011-01-01 00:00:00'
tA2 = '2011-08-29 00:00:00'
tB1 = '2011-08-30 00:00:00'
tB2 = '2011-12-31 00:00:00'
tC1 = '2012-01-01 00:00:00'
tC2 = '2012-09-17 00:00:00'
tD1 = '2012-09-18 00:00:00'
tD2 = '2012-12-31 00:00:00'
tE1 = '2013-01-01 00:00:00'
tE2 = '2013-03-01 00:00:00'
tF1 = '2015-03-03 00:00:00'
tF2 = '2015-06-18 00:00:00'
tG1 = '2015-06-19 00:00:00'
tG2 = '2015-12-31 00:00:00'
tH1 = '2016-01-01 00:00:00'
tH2 = '2016-07-07 00:00:00'
tI1 = '2016-07-07 00:00:00'
tI2 = '2016-07-18 20:00:00'
tJ1 = '2016-07-18 20:10:00'
tJ2 = '2016-12-31 00:00:00'

#tX1 = '2017-01-01 00:00:00'

tX1 = '2017-04-28 00:00:00'
tX2 = '2017-06-02 00:00:00'
tY1 = '2017-06-03 00:00:00'
tY2 = '2017-07-01 00:00:00'
tZ1 = '2017-07-02 00:00:00'
tZ2 = '2017-08-04 00:00:00'


tV1 = '2017-08-05 00:00:00'
tV2 = '2017-08-10 08:00:00'

tW1 = '2017-08-10 08:10:00'
tW2 = '2017-08-16 08:00:00'

parameter='delay_time'
# mean_temperatures, mean_risetimes=rise_time_vs_temperature.analyse_all(beam, magnet, tA1, tA2, 'rise_time', show_plots = True)
# mean_temperaturesB, mean_risetimesB=rise_time_vs_temperature.analyse_all(beam, magnet, tB1, tB2, 'rise_time', show_plots = True)
# mean_temperaturesC, mean_risetimesC=rise_time_vs_temperature.analyse_all(beam, magnet, tC1, tC2, 'rise_time', show_plots = True)
# mean_temperaturesD, mean_risetimesD=rise_time_vs_temperature.analyse_all(beam, magnet, tD1, tD2,'rise_time', show_plots = True )
# mean_temperaturesE, mean_risetimesE=rise_time_vs_temperature.analyse_all(beam, magnet, tE1, tE2, 'rise_time', show_plots = True)
# mean_temperaturesF, mean_risetimesF=rise_time_vs_temperature.analyse_all(beam, magnet, tF1, tF2, 'rise_time', show_plots = True)
# mean_temperaturesG, mean_risetimesG=rise_time_vs_temperature.analyse_all(beam, magnet, tG1, tG2, 'rise_time', show_plots = True)
# mean_temperaturesH, mean_risetimesH=rise_time_vs_temperature.analyse_all(beam, magnet, tH1, tH2, 'rise_time', show_plots = True)
# mean_temperaturesI, mean_risetimesI=rise_time_vs_temperature.analyse_all(beam, magnet, tI1, tI2, 'rise_time', show_plots = True)
# mean_temperaturesJ, mean_risetimesJ=rise_time_vs_temperature.analyse_all(beam, magnet, tJ1, tJ2, 'rise_time', show_plots = True)
mean_temperaturesX, mean_risetimesX=rise_time_vs_temperature.analyse_all(beam, magnet, tX1, tX2, parameter, show_plots = False)
mean_temperaturesY, mean_risetimesY=rise_time_vs_temperature.analyse_all(beam, magnet, tY1, tY2, parameter, show_plots = False)
mean_temperaturesZ, mean_risetimesZ=rise_time_vs_temperature.analyse_all(beam, magnet, tZ1, tZ2, parameter, show_plots = False)
mean_temperaturesV, mean_risetimesV=rise_time_vs_temperature.analyse_all(beam, magnet, tV1, tV2, parameter, show_plots = False)
mean_temperaturesW, mean_risetimesW=rise_time_vs_temperature.analyse_all(beam, magnet, tW1, tW2, parameter, show_plots = False)
# plt.figure(figsize=(12,6))
# plt.plot(mean_temperatures,mean_risetimes, 'o',  color='dodgerblue', markersize=8, markeredgecolor='b', markeredgewidth=1.0, label=tA1 + ' to ' + tA2)
# plt.plot(mean_temperaturesB,mean_risetimesB, 'o',   color='lightgreen', markersize=8,  markeredgecolor='forestgreen', markeredgewidth=1.0, label=tB1 + ' to ' + tB2)
# plt.plot(mean_temperaturesC,mean_risetimesC, 'o',  color='gold', markersize=8, markeredgecolor='darkorange', markeredgewidth=1.0, label=tC1 + ' to ' + tC2)
# plt.plot(mean_temperaturesD,mean_risetimesD, 'o',  color='plum',markersize=8, markeredgecolor='magenta', markeredgewidth=1.0, label=tD1 + ' to ' + tD2)
# plt.plot(mean_temperaturesE,mean_risetimesE, 'o', color='lightcoral', markersize=8, markeredgecolor='red', markeredgewidth=1.0, label=tE1 + ' to ' + tE2)
# plt.legend()
# plt.title('MKI8D 2011-2013')
# pytimber.set_xaxis_date()
# plt.show()



# plt.figure(figsize=(12,6))
# plt.plot(mean_temperaturesF,mean_risetimesF, 'o',  color='darkorchid',markersize=8, markeredgecolor='rebeccapurple', markeredgewidth=1.0, label=tF1 + ' to ' + tF2)
# plt.plot(mean_temperaturesG,mean_risetimesG, 'o', color='silver', markersize=8, markeredgecolor='grey', markeredgewidth=1.0, label=tG1 + ' to ' + tG2)
# plt.plot(mean_temperaturesH,mean_risetimesH, 'o',  color='powderblue',markersize=8, markeredgecolor='skyblue', markeredgewidth=1.0, label=tH1 + ' to ' + tH2)
# plt.plot(mean_temperaturesI,mean_risetimesI, 'o', color='dodgerblue', markersize=8, markeredgecolor='mediumblue', markeredgewidth=1.0, label=tI1 + ' to ' + tI2)
# plt.plot(mean_temperaturesJ,mean_risetimesJ, 'o',  color='royalblue',markersize=8, markeredgecolor='navy', markeredgewidth=1.0, label=tJ1 + ' to ' + tJ2)
# plt.legend()
# plt.title('MKI8D 2015-2016')
# pytimber.set_xaxis_date()
# plt.show()

plt.figure(figsize=(12,6))
plt.plot(mean_temperaturesX,mean_risetimesX, 'o',  color='dodgerblue', markersize=8, markeredgecolor='b', markeredgewidth=1.0, label=tX1 + ' to ' + tX2)
plt.plot(mean_temperaturesY,mean_risetimesY, 'o',   color='lightgreen', markersize=8,  markeredgecolor='forestgreen', markeredgewidth=1.0, label=tY1 + ' to ' + tY2)#
plt.plot(mean_temperaturesZ,mean_risetimesZ, 'o', color='gold', markersize=8, markeredgecolor='darkorange', markeredgewidth=1.0, label=tZ1 + ' to ' + tZ2)
plt.plot(mean_temperaturesV,mean_risetimesV, 'o', color='plum',markersize=8, markeredgecolor='magenta', markeredgewidth=1.0, label=tV1 + ' to ' + tV2)
plt.plot(mean_temperaturesW,mean_risetimesW, 'o',  color='lightcoral', markersize=8, markeredgecolor='red', markeredgewidth=1.0, label=tW1 + ' to ' + tW2)
plt.legend()
plt.title('MKI8D 2017: '+ parameter)
ax=plt.gca()
ax.set_xlabel(r'Temperature [$^\circ$C]')
ax.set_ylabel(parameter + '[$\mu$s]')
plt.show()

